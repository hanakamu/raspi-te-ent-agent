# raspi-te-ent-agent

ThousandEyes Enterprise Agent on Raspberry Pi.

## Getting started
### Requirements
- Hardware: Raspberry Pi 4 
  4GB Memory Model is recommended.
- OS: Ubuntu 20.04 LTS
  We tested on latest ubuntu image (20.04.3).
- Git
  You can get installation scripts and config samples from git repository. 
- Docker & Docker-compose
  TE agent will be run as a container. 
  It is managed with docker-compose for healing.
  These software will be installed automatically via the install shell script (install.sh).

### Installation Workflow with Existing Machine
1. Clone this repository to your Raspberry Pi.
2. Set configurable variables on a yaml file named "config.yml"  
   Please see "sample-config.yml" for gettig more detail.
3. Run install.sh  
   Please check the requirements before running.
4. Reboot your machine
   *This is OPTIONAL flow.  
   You can confirm a TE Agent will start automatically at system boot.

### *TBD: Installation Workflow on New Machine
1. Clone this repository to your PC  
1. Set configurable variables on a yaml file named "config.yml"  
   Please see "sample-config.yml" for gettig more detail.

## Roadmap

## Author
hanakamu

## License
MIT