#!/bin/bash
# Shell Script for Installing a TE Agent on Raspberry Pi
# https://gitlab.com/hanakamu/raspi-te-ent-agent

#--- Step1: Install Docker Engine via APT ---#
echo "⚙️ Step 1: Install Docker"

echo "🗑️ Removing old versions ..."
apt-get remove docker docker-engine docker.io containerd runc
echo "✅ Old versions are removed"

echo "📦 Installing prerequired sofwares ..."
apt update  -y
apt install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
echo "✅ Prerequired sofwares are installed"

echo "🔑 Adding Docker's official GPG key ..."
DOCKER_GPG_KEY="/usr/share/keyrings/docker-archive-keyring.gpg"
rm $DOCKER_GPG_KEY
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o $DOCKER_GPG_KEY
echo "✅ GPG Key is added"

echo "📦 Set up the repository (ARM64) ..."
echo \
  "deb [arch=arm64 signed-by=$DOCKER_GPG_KEY] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
echo "✅ Repository is set up"

echo "🐳 Installing Docker Engine ..."
apt-get update -y
apt-get install -y docker-ce docker-ce-cli containerd.io
systemctl enable docker
echo "✅ Docker Engine is installed"
docker --version


#--- Step2: Install Docker-Compose ---#
echo "⚙️ Step 2: Install Docker-Compose"

echo "🐳 Installing Docker Compose ..."
apt install -y \
    libffi-dev \
    libssl-dev \
    python3-dev \
    python3 \
    python3-pip
pip3 install docker-compose
echo "✅ Docker Compose is installed"
docker-compose --version


#--- Step3: Run TE Enterprise Agent with Docker-Compose ---#
echo "⚙️ Step 3: Run TE Enterprise Agent"

echo "🐳 Pulling container image of ThousandEyes Enterprise Agent (latest) ..."
docker pull thousandeyes/enterprise-agent
echo "✅ TE Agent is installed"

echo "👁️ Run TE Agent with Docker-Compose ..."
docker-compose up -d
echo "✅ TE Agent is starting..."


#--- Message ---#
echo "All installation steps are finished."
echo "When TE Agent will be availabe, you can access to the web-gui from your browser."
echo "Enjoy your full stack obserbable👀 (Dev&Ops) life❤️"

exit 0